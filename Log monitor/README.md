# Log monitor

The [log monitor for Python](https://trustedtwin.com/docs/tools/usage_monitor.html) allows you to gain direct access to the log content as it is recorded.

## Prerequisites

The log monitor program requires:
- Python 3.6 or above.
- The `trustedtwin` library (see [Trusted Twin Python library](https://trustedtwin.com/docs/libraries/library-python.html)).

## Installation

To install the log monitor program, download the `log_monitor.py` file. Next, add the name of the file and your [User Secret (API key)](https://trustedtwin.com/docs/reference/user-authentication/) to install and run the program:

```shell
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python log_monitor.py $USER_SECRET
```

## Optional arguments

It is possible to use additional, optional arguments.

| Optional argument | Description                            | 
|:------------------|:---------------------------------------|
| `-u` or `--url`   | Uses the URL provided in the argument. |
| `-f` or `--file`  | Adds the logs to the file indicated.   | 

## Copyright and license

Copyright 2022 Trusted Twin. All rights reserved.
     
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)
     
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
