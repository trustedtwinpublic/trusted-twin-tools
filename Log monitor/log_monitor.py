"""Log monitor for Trusted Twin Digital Twin developer platform."""
import sys
import signal
import json

from contextlib import redirect_stdout
from typing import Any, List, Tuple, Optional
from uuid import uuid4
from datetime import datetime, timezone
from time import sleep, time
from argparse import ArgumentParser, Namespace

from trustedtwin.tt_api import TTRESTService

# Version info
VERSION = '01.00.00'

# User log template:
#   - timestamp:    Expected format as for calling datetime.now(timezone.utc).isoformat()
#   - level:        ERR | WARN
#   - method:       Operation name similar to used in user role statement (eg. get_log)
#   - job UUID:     From the task
#   - message:      Log message string
#   - data:         JSON string with message details
TT_LOG_CACHE_MSG_TEMPLATE = '\r{} | {:<4s} | {} | {} | {} | {}'

# Log messages fetched by single read to the cache.
TT_LOG_CACHE_READ_SIZE = 100

# Log sleep time before calls to get_log.
TT_LOG_CACHE_SLEEP = 1

# Defaults
DEFAULT_URL = 'https://rest.trustedtwin.com'


class TextColors:
    """Text Colors"""
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    @classmethod
    def map(cls, level: str) -> str:
        return {
            'INFO': cls.OKGREEN,
            'WARN': cls.WARNING,
            'ERR': cls.FAIL
        }.get(level, cls.ENDC)


# Log running indicator
TT_LOG_ANIMATION = '-\|/'
TT_LOG_ANIMATION_TEMPLATE = TextColors.OKCYAN + "... [{}] ... ({:.3f}s)" + TextColors.ENDC


def printLog(message: str, colors: bool = True) -> None:
    """Prints log message"""
    parts = message.split("|", maxsplit=6)

    json_part = parts.pop(-1).strip()
    try:
        json_dict = json.loads(json_part)
    except:
        json_dict = {"msg": json_part}

    if colors:
        parts[1] = TextColors.map(parts[1].strip()) + parts[1] + TextColors.ENDC
        parts[4] = TextColors.BOLD + parts[4] + TextColors.ENDC

    print("|".join(parts))

    if json_dict:
        print(json.dumps(json_dict, indent=4))


def get_logs(service: TTRESTService, fragment: Optional[str] = None) -> Tuple[Optional[str], List[str]]:
    """Calls TrustedTwin API"""
    _, res_str = service.get_log(params={"fragment": fragment})
    result = json.loads(res_str)

    return result['fragment'], result['messages']


class SignalHandler:
    """Catches termination signal and sets exit variable"""

    def __init__(self):
        self.signal_received = False

        signal.signal(signal.SIGINT, self.set_signal_received)
        signal.signal(signal.SIGTERM, self.set_signal_received)

    def set_signal_received(self, num: Any, frame: Any) -> None:
        """Sets variable informing that signal was caught"""

        self.signal_received = True


def parse_args() -> Namespace:
    """Parse arguments for command line use."""
    parser = ArgumentParser(
        description=f'Trusted Twin log reader v{VERSION} by Trusted Twin (https://www.trustedtwin.com)')

    parser.add_argument('auth', help='Trusted Twin API Key', type=str)
    parser.add_argument('-u', '--url', help='API URL', type=str, default=DEFAULT_URL)
    parser.add_argument('-f', '--file', help='Log to file', type=str, default="")

    return parser.parse_args()


def execute() -> None:
    """Main loop"""
    args = parse_args()

    handler = SignalHandler()
    service = TTRESTService(tt_auth=args.auth, tt_base=args.url)

    new_fragment: Optional[str] = str(uuid4())

    try:
        ts_s = time()
        act_fragment, messages = get_logs(service, new_fragment)
        ts_e = time()
    except:
        printLog(TT_LOG_CACHE_MSG_TEMPLATE.format(datetime.now(timezone.utc).isoformat(), "ERR ", "get_log", "n/a",
                                                  "... Failed to connect to Trusted Twin API: [{}] ...".format(args.url), "{}"))
        return

    output = None
    screen = True
    position = 0
    if args.file:
        output = open(args.file, 'ta', encoding="utf-8")
        screen = False

    try:
        with redirect_stdout(output or sys.stdout):
            if not act_fragment:
                printLog(TT_LOG_CACHE_MSG_TEMPLATE.format(datetime.now(timezone.utc).isoformat(), "INFO", "get_log", "n/a",
                                                          "... Trusted Twin logging service started ...", "{}"), screen)
            else:
                printLog(TT_LOG_CACHE_MSG_TEMPLATE.format(datetime.now(timezone.utc).isoformat(), "WARN", "get_log", "n/a",
                                                          "... Trusted Twin logging service already running ...", "{}"), screen)

            while not handler.signal_received:
                for message in messages:
                    printLog(message, screen)

                if len(messages) < TT_LOG_CACHE_READ_SIZE:
                    if screen:
                        print(TT_LOG_ANIMATION_TEMPLATE.format(TT_LOG_ANIMATION[position], ts_e - ts_s), end="\r")
                        position = (position + 1) % len(TT_LOG_ANIMATION)

                    sleep(TT_LOG_CACHE_SLEEP)

                old_fragment = new_fragment
                new_fragment = str(uuid4())
                ts_s = time()
                act_fragment, messages = get_logs(service, new_fragment)
                ts_e = time()

                if not act_fragment:
                    printLog(TT_LOG_CACHE_MSG_TEMPLATE.format(datetime.now(timezone.utc).isoformat(), "ERR", "get_log", "n/a",
                                                              "... Trusted Twin logging service restarted (unexpected) ...", "{}"), screen)

                if old_fragment != act_fragment:
                    printLog(TT_LOG_CACHE_MSG_TEMPLATE.format(datetime.now(timezone.utc).isoformat(), "WARN", "get_log", "n/a",
                                                              "... Trusted Twin logging service missing fragment (another client is working) ...", "{}"), screen)

            printLog(TT_LOG_CACHE_MSG_TEMPLATE.format(datetime.now(timezone.utc).isoformat(), "INFO", "get_log", "n/a",
                                                      "... Trusted Twin logging service terminated ...", "{}"), screen)
    finally:
        if output:
            output.close()


if __name__ == '__main__':
    execute()
