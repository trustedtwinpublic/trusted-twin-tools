"""Ping for Trusted Twin Digital Twin developer platform."""
from datetime import datetime, timezone
import signal
import json

from typing import Callable, Dict, Any
from time import sleep, time
from urllib.parse import quote
from argparse import ArgumentParser, Namespace
from requests import Session

# Version info
VERSION = "01.00.00"

class TextColors:
    """Text Colors"""

    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


TT_PING_MSG_TEMPLATE = (
    "\rSEQ: {:d} [{:s}] -> GET in "
    + TextColors.BOLD
    + "{:.3f}s"
    + TextColors.ENDC
    + " [min {:.3f}s, max {:.3f}s] UPDATE in "
    + TextColors.BOLD
    + "{:.3f}s"
    + TextColors.ENDC
    + " [min {:.3f}s, max {:.3f}s]"
)

TT_PING_SLEEP = 10

DEFAULT_URL = "https://rest.trustedtwin.com"

# Ping history Twin Identity
TT_PING_TWIN_ID = "TOOLS#PING_HISTORY_TWIN_{}"
TT_PING_TWIN_TYPE = "Ping history"

DEFAULT_HISTORY = "1D"


def parse_args() -> Namespace:
    """Parse arguments for command line use."""
    parser = ArgumentParser(description=f"Trusted Twin PING v{VERSION} by Trusted Twin (https://www.trustedtwin.com)")

    parser.add_argument("auth", help="Trusted Twin API Key", type=str)
    parser.add_argument("-u", "--url", help="API URL", type=str, default=DEFAULT_URL)

    return parser.parse_args()


_args = parse_args()
_session = Session()


def _call(method: Callable, url: str, body: str) -> Dict:
    """Call HTTPS request"""
    response = method(url=url, data=body, headers={"Authorization": _args.auth})

    assert response, "RESPONSE is None"
    assert response.status_code in [200, 201], "RESPONSE [{}]: {}, {}".format(datetime.now(timezone.utc).isoformat(), response, response.json())

    return response.json()


def _twin_create(description: Dict[str, Any]) -> Dict:
    """Call twin create endpoint"""
    return _call(
        method=_session.post,
        url=_args.url + "/twins",
        body=json.dumps({"description": description}),
    )


def _ledger_add(twin: str, entries: Dict[str, Any]) -> Dict:
    """Call ledger add endpoint"""
    return _call(
        method=_session.post,
        url=_args.url + "/twins/{}/ledgers/personal".format(twin),
        body=json.dumps({"entries": entries}),
    )


def _ledger_update(twin: str, entries: Dict[str, Any]) -> Dict:
    """Call ledger update endpoint"""
    return _call(
        method=_session.patch,
        url=_args.url + "/twins/{}/ledgers/personal".format(twin),
        body=json.dumps({"entries": entries}),
    )


def _ledger_get(twin: str) -> Dict:
    """Call ledger get endpoint"""
    return _call(
        method=_session.get,
        url=_args.url + "/twins/{}/ledgers/personal".format(twin),
        body=None,
    )


def _identity_resolve(identity: str) -> Dict:
    """Call Identity resolve endpoint"""
    return _call(
        method=_session.get,
        url=_args.url + "/resolve/{}".format(quote(identity)),
        body=None,
    )


def _identity_create(twin: str, identity: str) -> Dict:
    """Call Identity add endpoint"""
    return _call(
        method=_session.post,
        url=_args.url + "/twins/{}/identities".format(twin),
        body=json.dumps({"identities": {identity: {}}}),
    )


def resolve_identity(fingerprint: str) -> str:
    """Resolves ping Twin Identity"""
    twin_id = TT_PING_TWIN_ID.format(fingerprint)

    ts_s = time()
    response = _identity_resolve(twin_id)
    ts_e = time()

    if response["twins"]:
        assert len(response["twins"]) == 1

        twin_uuid = response["twins"][0]

        print(TextColors.OKGREEN + "Ping history Twin resolved [{}] ({:.3f}s) ...".format(twin_uuid, ts_e - ts_s) + TextColors.ENDC)
    else:
        ts_s = time()

        twin_uuid = _twin_create(description={"type": TT_PING_TWIN_TYPE})["creation_certificate"]["uuid"]

        _identity_create(twin=twin_uuid, identity=twin_id)

        ts_e = time()

        _ledger_add(
            twin=twin_uuid,
            entries={
                "get_time": {"history": DEFAULT_HISTORY, "value": None},
                "update_time": {"history": DEFAULT_HISTORY, "value": None},
                "get_min_max": {"value": [999.0, 0.0]},
                "update_min_max": {"value": [999.0, 0.0]},
                "count": {"value": 1},
            },
        )

        print(TextColors.OKGREEN + "Ping history Twin created [{}] ({:.3f}s) ...".format(twin_uuid, ts_e - ts_s) + TextColors.ENDC)

    return twin_uuid


class SignalHandler:
    """Catches termination signal and sets exit variable"""

    def __init__(self):
        self.signal_received = False

        signal.signal(signal.SIGINT, self.set_signal_received)
        signal.signal(signal.SIGTERM, self.set_signal_received)

    def set_signal_received(self, num: Any, frame: Any) -> None:
        """Sets variable informing that signal was caught"""

        self.signal_received = True


_handler = SignalHandler()


def execute() -> None:
    """Main loop"""

    twin_uuid = resolve_identity(_args.auth[-4:])

    update_ts_s = 0.0
    update_ts_e = 0.0

    # g_min_max = None
    # u_min_max = None

    g_min_max = [999.0, 0.0]
    u_min_max = [999.0, 0.0]

    starting = True

    while not _handler.signal_received:
        get_ts_s = time()
        response = _ledger_get(twin_uuid)
        get_ts_e = time()

        if starting:
            print()
            print(TextColors.BOLD + "Ping twin content:" + TextColors.ENDC)
            print(json.dumps(response, indent=4))
            print()

            starting = False

        count = response["entries"]["count"]["value"] + 1

        if g_min_max is None:
            g_min_max = response["entries"]["get_min_max"]["value"]
        if u_min_max is None:
            u_min_max = response["entries"]["update_min_max"]["value"]

        update_ts_s_new = time()
        response = _ledger_update(
            twin=twin_uuid,
            entries={
                "get_time": {"value": get_ts_e - get_ts_s},
                "update_time": {"value": (update_ts_e - update_ts_s) if update_ts_e > 0 else None},
                "get_min_max": {"value": g_min_max},
                "update_min_max": {"value": u_min_max},
                "count": {"value": count},
            },
        )
        update_ts_e = time()
        update_ts_s = update_ts_s_new

        g_min_max[0] = min(g_min_max[0], get_ts_e - get_ts_s)
        g_min_max[1] = max(g_min_max[1], get_ts_e - get_ts_s)

        u_min_max[0] = min(u_min_max[0], update_ts_e - update_ts_s)
        u_min_max[1] = max(u_min_max[1], update_ts_e - update_ts_s)

        print(
            TT_PING_MSG_TEMPLATE.format(
                count,
                datetime.now(timezone.utc).isoformat(),
                get_ts_e - get_ts_s,
                g_min_max[0],
                g_min_max[1],
                update_ts_e - update_ts_s,
                u_min_max[0],
                u_min_max[1],
            )
        )

        sleep(TT_PING_SLEEP)


if __name__ == "__main__":
    execute()
