# Ping

The [Ping tool](https://trustedtwin.com/docs/tools/ping.html) periodically performs the [get_twin](https://trustedtwin.com/docs/reference/twin/get-a-twin.html) and [update_twin](https://trustedtwin.com/docs/reference/twin/update-a-twin.html) operations to establish the minimum and maximum processing times of these operations as well as the processing time of the last operation. The requests are performed in a separate Twin created for the purpose of establishing the processing times of the operations.

## Prerequisites

The Ping program requires:
- Python 3.6 or above.
- The `requests` library.

## Installation

To install the Ping monitor program, download the `ping.py` file. Next, add the name of the file and your [User Secret (API key)](https://trustedtwin.com/docs/reference/user-authentication/) to install and run the program:

```shell
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python ping.py $USER_SECRET
```

## Optional arguments

It is possible to use additional, optional arguments.

| Optional argument | Description                            | 
|:------------------|:---------------------------------------|
| `-u` or `--url`   | Uses the URL provided in the argument. |

## Copyright and license

Copyright 2022 Trusted Twin. All rights reserved.
     
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)
     
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.




