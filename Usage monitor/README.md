# Usage monitor

The [usage monitor for Python](https://trustedtwin.com/docs/tools/usage_monitor.html) allows you to monitor Account and User [usage](https://trustedtwin.com/docs/reference/usage/).

## Prerequisites

The Usage monitor program requires:
- Python 3.6  or above.
- The `requests` library.
- `curses` Python module
- For Unix-based OS: curses system library, e.g. ncurses
- For Windows: additional Python library: `windows-curses`

## Installation

To install the usage monitor program, download the `usage_monitor.py` , `usage.py`, and `usage_monitor.json` files. Next, add the name of the `usage_monitor.py` file and your [User Secret (API key)](https://trustedtwin.com/docs/reference/user-authentication/) to install and run the program:

```shell
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python usage_monitor.py $USER_SECRET
```

## Copyright and license

Copyright 2022 Trusted Twin. All rights reserved.
     
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)
     
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.




