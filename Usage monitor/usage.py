"""Usage class for reading data from Trusted Twin platform"""
from typing import Dict, Optional
from time import time
from requests import Session

DEFAULT_URL = "https://rest.trustedtwin.com"


class TTUsage:
    """Usage delta"""

    def __init__(self, secret: str, user: Optional[str] = None, url: str = DEFAULT_URL):
        """Creates an object"""
        self._url = url
        self._header = {"Authorization": secret}
        self._session = Session()
        self._api_time: float = 0.0
        self._api_count: int = 0

        self.reset_ts = time()

        self._act_account_usage: Dict[str, Dict[str, float]] = {}
        self._act_user_usage: Dict[str, Dict[str, float]] = {}

        self.account_ts: float = self.reset_ts
        self.account_usage: Dict[str, float] = {}
        self.account_delta: Dict[str, float] = {}

        self.user_ts: float = self.reset_ts
        self.user_usage: Dict[str, float] = {}
        self.user_delta: Dict[str, float] = {}

        whoami = self._api_call(url="{}/whoami".format(url))

        self.user = user or whoami.get("user")
        self.account = whoami.get("account")

    def _api_call(self, url: str) -> Dict[str, Dict]:
        """Calls TT API"""
        ts_start = time()
        result = self._session.get(url=url, headers=self._header)
        self._api_time += time() - ts_start
        self._api_count += 1

        return result.json()

    def _get_account_usage(self) -> Dict[str, Dict[str, float]]:
        """Reads account usage from TT API"""
        return self._api_call(url="{url}/usage".format(url=self._url)).get("usage")

    def _get_user_usage(self) -> Dict[str, Dict[str, float]]:
        """Reads user usage from TT API"""
        return self._api_call(url="{url}/usage/{user}".format(url=self._url, user=self.user)).get("usage")

    @staticmethod
    def _calculate_usage(usage_new: Dict[str, Dict[str, float]], usage_act: Dict[str, Dict[str, float]]) -> Dict[str, float]:
        """Returns delta between usage_new and usage_act"""
        usage_delta: Dict[str, float] = {}

        for bucket, usage_new_data in usage_new.items():
            if bucket not in usage_act:
                usage_act[bucket] = {}

            usage_act_data = usage_act[bucket]

            for item, value in usage_new_data.items():
                delta = value - usage_act_data.get(item, 0.0)

                if delta != 0.0:
                    usage_delta[item] = usage_delta.get(item, 0.0) + delta
                    usage_act_data[item] = value

        return usage_delta

    @staticmethod
    def _update_usage(usage: Dict[str, float], delta: Dict[str, float]):
        """Updates usage with delta"""
        for item, value in delta.items():
            if item not in usage:
                usage[item] = 0.0

            usage[item] += value

    def update(self):
        """Reads data from Trusted Twin API and updates usage"""
        now = time()

        # Processing Account data
        usage_delta = self._calculate_usage(self._get_account_usage(), self._act_account_usage)
        if usage_delta:
            self.account_ts = now
            self.account_delta = usage_delta
            self._update_usage(self.account_usage, usage_delta)

        # Processing User data
        usage_delta = self._calculate_usage(self._get_user_usage(), self._act_user_usage)
        if usage_delta:
            self.user_ts = now
            self.user_delta = usage_delta
            self._update_usage(self.user_usage, usage_delta)

    def reset(self):
        """Resets usage"""
        self.reset_ts = time()

        self._act_account_usage = self._get_account_usage()
        self._act_user_usage = self._get_user_usage()

        self.account_ts = self.reset_ts
        self.account_usage = {}
        self.account_delta = {}

        self.user_ts = self.reset_ts
        self.user_usage = {}
        self.user_delta = {}
