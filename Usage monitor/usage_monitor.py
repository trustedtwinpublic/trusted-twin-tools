"""Usage monitor for Trusted Twin Digital Twin developer platform."""
import os
import json
import curses

from typing import Callable, List, Set, Dict, Optional
from datetime import datetime
from time import sleep, time
from json.decoder import JSONDecodeError
from argparse import ArgumentParser, Namespace
from curses import wrapper, window
from curses.textpad import rectangle

from usage import TTUsage, DEFAULT_URL

_VERSION = "01.00.00"

_MIN_PANEL_HEIGHT = 9
_MIN_PANEL_WIDTH = 90
_MIN_WIN_HEIGHT = _MIN_PANEL_HEIGHT + 4 + 2
_MIN_WIN_WIDTH = _MIN_PANEL_WIDTH + 2

_DISP_LEN = 21
_DISP_GAP = 5

_TEXT_NORMAL = 9
_TEXT_HEADER = 16
_TEXT_NUMBERS = 16
_TEXT_ANIM = 3

_ANIMATION = "-\|/"
_ANIMATION_TEMPLATE = "({count:,d} API calls, {delay:.3f}s/call) [{anim}]"

_TOP_LINE_TEMPLATE = "Trusted Twin usage monitor ver. {}".format(_VERSION)
_BOTTOM_LINE_TEMPLATE = "[R]-Refresh  [A]-Auto ({auto_state})  [C]-Reset  [X]-Exit"

_API_CALL_INTERVAL = 10.000
_GET_KEY_INTERVAL = 0.100

_script_path, _script_name = os.path.split(os.path.realpath(__file__))
_script_name, _ = os.path.splitext(_script_name)


_GROUPS: Dict[str, List[str]] = {"requests": ["rest_count", "request_count"]}

try:
    with open(os.path.join(_script_path, _script_name + ".json"), "r", encoding="utf-8") as file:
        try:
            _GROUPS = json.load(file)
        except JSONDecodeError:
            pass
except FileNotFoundError:
    pass

_ITEMS: List[str] = []
for items in _GROUPS.values():
    _ITEMS.extend(items)

assert len(_ITEMS) == len(set(_ITEMS)), "Accounting item name duplicated"
assert len(_GROUPS) == len(set(_GROUPS)), "Group name duplicated"


def parse_args() -> Namespace:
    """Parse arguments for command line use."""
    parser = ArgumentParser(description=f"Trusted Twin usage monitor v{_VERSION} by Trusted Twin (https://www.trustedtwin.com)")

    parser.add_argument("auth", help="Trusted Twin API Key", type=str)
    parser.add_argument("-u", "--url", help="Trusted Twin API URL", type=str, default=DEFAULT_URL)
    parser.add_argument("-m", "--user", help="Monitored user UUID", type=str, default=None)

    return parser.parse_args()


class Pads:
    """Screen configuration"""

    def __init__(self, stdscr: window, usage: TTUsage, groups: Dict[str, List[str]], values: Optional[Callable] = None) -> None:
        """Initializes a new set of pads"""
        self.usage = usage
        self.groups = groups

        self._values = values

        self._act_height, self._act_width = stdscr.getmaxyx()

        self._act_win_height = max(self._act_height, _MIN_WIN_HEIGHT)
        self._act_win_width = max(self._act_width, _MIN_WIN_WIDTH)

        self._act_panel_height = self._act_win_height - 2

        self.account_pad = curses.newpad(self._act_panel_height // 3, self._act_win_width * 2)
        self.user_pad = curses.newpad(self._act_panel_height - self._act_panel_height // 3, self._act_win_width * 2)
        self.top_line = curses.newpad(1, self._act_win_width + 1)
        self.bottom_line = curses.newpad(1, self._act_win_width + 1)

        self.account_pad.bkgd(" ", curses.color_pair(_TEXT_NORMAL))
        self.user_pad.bkgd(" ", curses.color_pair(_TEXT_NORMAL))
        self.top_line.bkgd(" ", curses.color_pair(_TEXT_NORMAL))
        self.bottom_line.bkgd(" ", curses.color_pair(_TEXT_NORMAL))

        self._animation: int = 0

    def _refresh_content(self, win: window, delta: Dict[str, float], usage: Dict[str, float]) -> Set[str]:
        """Displays usage. Returns set of unknown accounting items."""
        _items = set(delta) | set(usage)
        _net_height, _ = win.getmaxyx()
        _net_height -= 2

        if _items:
            max_item_len = max(len(item) for item in _items)
        else:
            max_item_len = 0

        coll_width = max_item_len + _DISP_LEN + _DISP_GAP

        act_coll = 0
        act_row = 0

        for name, group in self.groups.items():
            header_row = act_row
            header_coll = act_coll

            for item in group:
                if item in _items:
                    _items.remove(item)

                    act_row += 1
                    if act_row == _net_height:
                        act_row = 0
                        act_coll += coll_width

                    if item.endswith(("time",)):
                        _format = "10,.3f"
                    else:
                        _format = "10,.0f"

                    if item in delta:
                        _data = "{value:{_format}} ".format(value=delta[item], _format=_format)
                    else:
                        _data = ""
                    _data += "{value:{_format}}".format(value=usage[item], _format=_format)

                    win.addstr(act_row + 1, act_coll + 1, item, curses.color_pair(_TEXT_NORMAL))
                    win.addstr(act_row + 1, act_coll + max_item_len + 1, _data.rjust(_DISP_LEN, " "), curses.color_pair(_TEXT_NUMBERS))

            if act_row != header_row or act_coll != header_coll:
                win.addstr(header_row + 1, header_coll + 1, name, curses.color_pair(_TEXT_HEADER) | curses.A_BOLD)

                act_row += 2
                if act_row >= _net_height - 1:
                    act_row = 0
                    act_coll += coll_width

        return _items

    def _refresh_border(self, win: window, title: str, update_ts: float) -> None:
        """Updates border of the window"""
        win_height, _ = win.getmaxyx()
        time_info = "[Change: {}]".format(datetime.fromtimestamp(update_ts).ctime())
        win.attron(curses.color_pair(_TEXT_HEADER))
        rectangle(win, 0, 0, win_height - 1, self._act_win_width - 1)
        win.addstr(0, 1, title)
        win.addstr(win_height - 1, self._act_win_width - len(time_info) - 1, time_info)

    def refresh_panels(self) -> None:
        """Refreshes panels"""
        self.account_pad.erase()
        self.user_pad.erase()

        self._refresh_content(win=self.account_pad, delta=self.usage.account_delta, usage=self.usage.account_usage)
        self._refresh_content(win=self.user_pad, delta=self.usage.user_delta, usage=self.usage.user_usage)
        self._refresh_border(win=self.account_pad, title="[ACCOUNT: {}]".format(self.usage.account), update_ts=self.usage.account_ts)
        self._refresh_border(win=self.user_pad, title="[USER: {}]".format(self.usage.user), update_ts=self.usage.user_ts)

        self.account_pad.refresh(0, 0, 1, 0, self._act_panel_height // 3, self._act_width - 1)
        self.user_pad.refresh(0, 0, self._act_panel_height // 3 + 1, 0, self._act_panel_height, self._act_width - 1)

    def refresh_lines(self) -> None:
        """Refreshes top and bottom lines"""
        anim_info = _ANIMATION_TEMPLATE.format(
            anim=_ANIMATION[self._animation], count=self.usage._api_count, delay=self.usage._api_time / self.usage._api_count
        )
        time_info = "Reset: {} ({} ago)".format(
            datetime.fromtimestamp(self.usage.reset_ts).ctime(), str(datetime.now() - datetime.fromtimestamp(self.usage.reset_ts)).split(".")[0]
        )

        self.top_line.erase()
        self.bottom_line.erase()

        self.top_line.addstr(0, self._act_win_width - len(time_info), time_info, curses.color_pair(_TEXT_NORMAL))
        self.bottom_line.addstr(0, self._act_win_width - len(anim_info), anim_info, curses.color_pair(_TEXT_ANIM))

        if self._values is not None:
            self.top_line.addstr(0, 0, _TOP_LINE_TEMPLATE.format(**self._values()), curses.color_pair(_TEXT_NORMAL))
            self.bottom_line.addstr(0, 0, _BOTTOM_LINE_TEMPLATE.format(**self._values()), curses.color_pair(_TEXT_HEADER))
        else:
            self.top_line.addstr(0, 0, _TOP_LINE_TEMPLATE, curses.color_pair(_TEXT_NORMAL))
            self.bottom_line.addstr(0, 0, _BOTTOM_LINE_TEMPLATE, curses.color_pair(_TEXT_HEADER))

        self.top_line.refresh(0, 0, 0, 0, 0, self._act_width - 1)
        self.bottom_line.refresh(0, 0, self._act_height - 1, 0, self._act_height - 1, self._act_width - 1)

        self._animation = (self._animation + 1) % len(_ANIMATION)

    def refresh(self, reset: bool = False) -> None:
        """Refreshes the panels"""
        if reset:
            self.usage.reset()
            unknown_items = {}
        else:
            unknown_items = self.usage.update()

        self.refresh_panels()
        self.refresh_lines()

        if unknown_items:
            curses.beep()
            curses.beep()
            curses.beep()

    def resize(self, stdscr: window) -> None:
        """Resizes the pads"""
        self._act_height, self._act_width = stdscr.getmaxyx()

        self._act_win_height = max(self._act_height, _MIN_WIN_HEIGHT)
        self._act_win_width = max(self._act_width, _MIN_WIN_WIDTH)

        self._act_panel_height = self._act_win_height - 2

        self.account_pad.resize(self._act_panel_height // 3, self._act_win_width * 2)
        self.user_pad.resize(self._act_panel_height - self._act_panel_height // 3, self._act_win_width * 2)
        self.top_line.resize(1, self._act_win_width + 1)
        self.bottom_line.resize(1, self._act_win_width + 1)


class Switch:
    """Basic switch class"""

    def __init__(self, name):
        """Init the switch"""
        self._name = name
        self._switch: bool = True

    def __call__(self) -> Dict:
        """Returns dict"""
        return {self._name: str(self)}

    def __str__(self) -> str:
        """Returns switch representation"""
        if self._switch:
            return "{:.0f}s".format(_API_CALL_INTERVAL)

        return "OFF"

    def __bool__(self) -> bool:
        """Returns switch value"""
        return self._switch

    def toggle(self) -> None:
        """Toggles the switch"""
        self._switch = not self._switch


_args = parse_args()


def main(stdscr: window):
    """Main loop"""
    usage = TTUsage(secret=_args.auth, url=_args.url, user=_args.user)
    usage.reset()

    curses.curs_set(0)
    if curses.has_colors():
        curses.start_color()
        curses.use_default_colors()

        for i in range(curses.COLORS):
            curses.init_pair(i + 1, i, -1)
            # stdscr.addstr(str(i) + " ", curses.color_pair(i))

    stdscr.nodelay(True)
    stdscr.erase()
    stdscr.bkgd(" ", curses.color_pair(1))
    stdscr.refresh()

    api_last_call: float = 0.0
    api_reset: bool = False
    api_auto: Switch = Switch("auto_state")

    pads = Pads(stdscr, usage, _GROUPS, api_auto)

    cmd = -1

    while True:
        if (api_last_call == 0.0) or (api_auto and time() - api_last_call >= _API_CALL_INTERVAL):
            pads.refresh(api_reset)
            api_last_call = time()
            api_reset = False

        cmd = stdscr.getch()

        # Refresh usage
        if cmd in [ord("r"), ord("R")]:
            api_last_call = 0.0

        # Reset usage
        elif cmd in [ord("c"), ord("C")]:
            api_last_call = 0.0
            api_reset = True

        # Toggle automatic API call on/off
        elif cmd in [ord("a"), ord("A")]:
            api_auto.toggle()
            if api_auto:
                api_last_call = 0.0
            else:
                pads.refresh_lines()

        # Exit script
        elif cmd in [ord("x"), ord("X")]:
            break

        # Resize window
        elif cmd == curses.KEY_RESIZE:
            api_last_call = 0.0
            pads.resize(stdscr)
            stdscr.refresh()

        # Unknown command
        elif cmd != -1:
            curses.beep()

        else:
            sleep(_GET_KEY_INTERVAL)


if __name__ == "__main__":
    wrapper(main)
